#!/bin/sh
#
# docker-entrypoint for service

set -e

echo "Executing java ${JAVA_ARGS} "$@""
java ${JAVA_ARGS} -jar eros-ingestion-service-0.0.1-SNAPSHOT.jar

