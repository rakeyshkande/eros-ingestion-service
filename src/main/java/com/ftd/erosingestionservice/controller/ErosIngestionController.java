package com.ftd.erosingestionservice.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;
import com.ftd.commons.logging.annotation.LogExecutionTime;
import com.ftd.erosingestionservice.api.request.ErosOrderRequest;
import com.ftd.erosingestionservice.api.request.OrderStatusRequest;
import com.ftd.erosingestionservice.api.response.ErosIngestionResponse;
import com.ftd.erosingestionservice.service.ErosOrderService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api/floristOrder")
public class ErosIngestionController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ErosIngestionController.class);

    @Autowired
    private ErosOrderService erosOrderService;

    @ApiOperation(value = "Saves (a) copy of EROS orders",
            notes = " This service receives acknowdledged orders from EROS and saves them to DB ")

    @Timed
    @ExceptionMetered
    @LogExecutionTime
    @RequestMapping(value = "/postOrders", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ErosIngestionResponse> postErosOrders(@Validated @RequestBody ErosOrderRequest orderRequest) {
        LOGGER.info("Received request for postErosOrders {}", orderRequest);

        ErosIngestionResponse postOrdersResponse = new ErosIngestionResponse();
        try {
            boolean response = erosOrderService.insertFloristOrders(orderRequest);
            postOrdersResponse.setErosIngestionStatus(response);

            if (response) {
                LOGGER.info("postErosOrders Response {}. messageId {}", postOrdersResponse,
                        orderRequest.getMessageId());
                return new ResponseEntity<>(postOrdersResponse, HttpStatus.OK);
            } else {
                LOGGER.error("postErosOrders Response {}. messageId {}", postOrdersResponse,
                        orderRequest.getMessageId());
                return new ResponseEntity<>(postOrdersResponse, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            LOGGER.error("postErosOrders exception {}. request {} ", ex, orderRequest);
            return new ResponseEntity<>(postOrdersResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @ApiOperation(value = "Saves EROS order status",
            notes = " This service receives order status from EROS and saves them to DB ")

    @Timed
    @ExceptionMetered
    @LogExecutionTime
    @RequestMapping(value = "/postOrderStatus", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ErosIngestionResponse> postOrderStatus(@Validated @RequestBody OrderStatusRequest
                                                                             orderStatusRequest) {
        LOGGER.info("Received postOrderStatus Request {}", orderStatusRequest);

        ErosIngestionResponse postOrderStatusResponse = new ErosIngestionResponse();
        try {
            boolean response = erosOrderService.updateFloristOrderStatus(orderStatusRequest);
            postOrderStatusResponse.setErosIngestionStatus(response);

            if (response) {
                LOGGER.info("postOrderStatus Response {}. messageId {}", postOrderStatusResponse,
                        orderStatusRequest.getMessageId());
                return new ResponseEntity<>(postOrderStatusResponse, HttpStatus.OK);
            } else {
                LOGGER.error("postOrderStatus Response {}. messageId {}", postOrderStatusResponse,
                        orderStatusRequest.getMessageId());
                return new ResponseEntity<>(postOrderStatusResponse, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            LOGGER.error("postErosOrders exception {}. request {}", ex, orderStatusRequest);
            return new ResponseEntity<>(postOrderStatusResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
