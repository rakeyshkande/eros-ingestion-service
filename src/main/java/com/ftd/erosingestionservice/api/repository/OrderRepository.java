package com.ftd.erosingestionservice.api.repository;

import com.ftd.erosingestionservice.api.entity.OrderEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface OrderRepository extends MongoRepository<OrderEntity, String> {

//    @Query("{ 'mainMemberCode': ?0, 'order.messageId': ?1, 'order.transmissionId': ?2 }")
//    OrderEntity findOrdersByMainMemberCodeAndMessageIdAndTransmissionId(String mainMemberCode,
//                                                                        String messageId,
//                                                                        String transmissionId);
    @Query("{'mainMemberCode':?0, 'order.messageId':?1}")
    ArrayList<OrderEntity> findOrderEntitiesByMainMemberCodeAndMessageId(
            String mainMemberCode, String messageId, Sort sort);
}
