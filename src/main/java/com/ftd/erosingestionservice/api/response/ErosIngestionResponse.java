package com.ftd.erosingestionservice.api.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonRootName("erosOrderRequest")
public class ErosIngestionResponse {

    @JsonProperty("erosIngestionStatus")
    private boolean erosIngestionStatus;
}
