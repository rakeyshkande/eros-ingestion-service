package com.ftd.erosingestionservice.api.entity;

import com.ftd.erosingestionservice.api.request.ErosOrderRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@ToString(exclude = "id")
@Document(collection = "orders")
public class OrderEntity {
    @Id
    private String id;
    private String mainMemberCode;
    private String erosOrderStatus;
    private boolean partnerConfirmStatus;
    private DateTime partnerConfirmDate;
    private boolean orderActive;
    private long cofId;
    private DateTime createdOn;
    private DateTime updatedOn;
    private ErosOrderRequest order;

    public OrderEntity(String mainMemberCode,
                       String erosOrderStatus,
                       boolean partnerConfirmStatus,
                       DateTime partnerConfirmDate,
                       boolean orderActive,
                       Long cofId,
                       DateTime createdOn,
                       DateTime updatedOn,
                       ErosOrderRequest order) {

        this.mainMemberCode = mainMemberCode;
        this.erosOrderStatus = erosOrderStatus;
        this.partnerConfirmStatus = partnerConfirmStatus;
        this.partnerConfirmDate = partnerConfirmDate;
        this.orderActive = orderActive;
        this.cofId = cofId;
        this.createdOn = createdOn;
        this.updatedOn = updatedOn;
        this.order = order;
    }
}
