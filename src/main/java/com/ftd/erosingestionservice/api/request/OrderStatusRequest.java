package com.ftd.erosingestionservice.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

@Getter
@Setter
@ToString
@JsonRootName("orderStatus")
public class OrderStatusRequest {

    @NotBlank
    @JsonProperty("mainMemberCode")
    private String mainMemberCode;
    @NotBlank
    @JsonProperty("messageId")
    private String messageId;
    @NotBlank
    @JsonProperty("messageStatus")
    private String messageStatus;
}
