package com.ftd.erosingestionservice.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

@Getter
@Setter
@ToString
@JsonRootName("erosOrderRequest")
public class ErosOrderRequest {

    @NotBlank
    @JsonProperty("mainMemberCode")
    private String mainMemberCode;
    @NotBlank
    @JsonProperty("messageId")
    private String messageId;
    @NotBlank
    @JsonProperty("transmissionId")
    private String transmissionId;
    @JsonProperty("messageReceivedDate")
    private String messageReceivedDate;
    @JsonProperty("messageStatus")
    private String messageStatus;
    @JsonProperty("referenceNumber")
    private String referenceNumber;
    @JsonProperty("orderSequenceNumber")
    private String orderSequenceNumber;
    @JsonProperty("deliveryDate")
    private String deliveryDate;
    @JsonProperty("occasionCode")
    private String occasionCode;
    @JsonProperty("deliveryDetails")
    private String deliveryDetails;
    @JsonProperty("firstChoiceProductCode")
    private String firstChoiceProductCode;
    @JsonProperty("firstChoiceProductDescription")
    private String firstChoiceProductDescription;
    @JsonProperty("secondChoiceProductCode")
    private String secondChoiceProductCode;
    @JsonProperty("secondChoiceProductDescription")
    private String secondChoiceProductDescription;
    @JsonProperty("price")
    private Double price;
    @JsonProperty("cardMessage")
    private String cardMessage;
    @JsonProperty("specialInstructions")
    private String specialInstructions;
    @JsonProperty("operator")
    private String operator;
    @JsonProperty("sender")
    private Florist sender;
    @JsonProperty("receiver")
    private Florist receiver;
    @JsonProperty("recipient")
    private Address recipient;
}
