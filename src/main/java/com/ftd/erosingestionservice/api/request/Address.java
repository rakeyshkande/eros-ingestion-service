package com.ftd.erosingestionservice.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Address {

    @JsonProperty("name")
    private String name;
    @JsonProperty("address")
    private String address;
    @JsonProperty("cityStateZip")
    private String cityStateZip;
    @JsonProperty("city")
    private String city;
    @JsonProperty("stateCode")
    private String stateCode;
    @JsonProperty("postalCode")
    private String postalCode;
    @JsonProperty("countryCode")
    private String countryCode;
    @JsonProperty("phone")
    private String phone;
}
