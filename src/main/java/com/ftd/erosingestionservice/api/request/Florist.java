package com.ftd.erosingestionservice.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Florist {
    @JsonProperty("memberCode")
    private String memberCode;
    @JsonProperty("memberClassification")
    private String memberClassification;
    @JsonProperty("businessName")
    private String businessName;
    @JsonProperty("address")
    private String address;
    @JsonProperty("city")
    private String city;
    @JsonProperty("stateCode")
    private String stateCode;
    @JsonProperty("postalCode")
    private String postalCode;
    @JsonProperty("phone")
    private String phone;
}
