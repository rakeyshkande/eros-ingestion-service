package com.ftd.erosingestionservice.service;


import com.ftd.erosingestionservice.api.request.ErosOrderRequest;
import com.ftd.erosingestionservice.api.request.OrderStatusRequest;

public interface ErosOrderService {

    boolean insertFloristOrders(ErosOrderRequest erosOrder);
    boolean updateFloristOrderStatus(OrderStatusRequest orderStatusRequest);
}
