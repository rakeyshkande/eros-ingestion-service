package com.ftd.erosingestionservice.service;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.ftd.erosingestionservice.api.entity.OrderEntity;
import com.ftd.erosingestionservice.api.repository.OrderRepository;
import com.ftd.erosingestionservice.api.request.ErosOrderRequest;
import com.ftd.erosingestionservice.api.request.OrderStatusRequest;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class ErosOrderServiceImpl implements ErosOrderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ErosOrderServiceImpl.class);

    @Autowired
    private OrderRepository orderRepository;


    @HystrixCommand(groupKey = "hystrixGroup", commandKey = "insertFloristOrdersCommand",
    threadPoolKey = "HystrixThreadPoolKey", fallbackMethod = "insertFloristOrdersFallback")
    @Override
    public boolean insertFloristOrders(ErosOrderRequest erosOrder) {
        LOGGER.info("Insert Florist Orders {}", erosOrder);

        boolean orderActive = false;
        long cofId = 0L;
        String mainMemberCode = erosOrder.getMainMemberCode();
        String messageStatus = erosOrder.getMessageStatus();
        erosOrder.setMainMemberCode(null);
        erosOrder.setMessageStatus(null);

        if (!messageStatus.isEmpty() && messageStatus.equals("SENT")) {
            orderActive = true;
        }

        if (erosOrder.getSpecialInstructions() != null && !erosOrder.getSpecialInstructions().isEmpty() &&
                erosOrder.getSender().getMemberClassification()
                        .toUpperCase().equals("FOL")) {
            cofId = extractCOFID(erosOrder.getSpecialInstructions(), erosOrder.getMessageId());
        }

        OrderEntity orderEntity = new OrderEntity(
                mainMemberCode,
                messageStatus,
                false,
                DateTime.now(),
                orderActive,
                cofId,
                DateTime.now(),
                DateTime.now(),
                erosOrder
        );

        OrderEntity success = orderRepository.save(orderEntity);
        LOGGER.info("InsertFloristOrders: Save Order to DB Response {}", success);
        return success.getId() != null;
    }



    @HystrixCommand(groupKey = "hystrixGroup", commandKey = "updateFloristOrderStatusCommand",
            threadPoolKey = "HystrixThreadPoolKey", fallbackMethod = "updateFloristOrderStatusFallback")
    @Override
    public boolean updateFloristOrderStatus(OrderStatusRequest orderStatusRequest) {
        LOGGER.info("update Florist Order status {}", orderStatusRequest);

        boolean orderActive = false;

        if (orderStatusRequest != null) {

            ArrayList<OrderEntity> orders = orderRepository.findOrderEntitiesByMainMemberCodeAndMessageId(
                    orderStatusRequest.getMainMemberCode(),
                    orderStatusRequest.getMessageId(),
                    new Sort(Sort.Direction.DESC, "order.transmissionId")
            );
            if (!orders.isEmpty() && orders.get(0) != null && orders.get(0).getId() != null) {

                OrderEntity order = orders.get(0);
                LOGGER.info("findOrderByMainMemberCodeAndMessageId {}", order);
                if (orderStatusRequest.getMessageStatus().toUpperCase().equals("SENT")) {
                    orderActive = true;
                }

                order.setOrderActive(orderActive);
                order.setErosOrderStatus(orderStatusRequest.getMessageStatus());
                order.setUpdatedOn(DateTime.now());

                orderRepository.save(order);
                LOGGER.info("update Florist Order Status success {}", order);
                return true;
            }
        }
        return false;
    }

    private long extractCOFID(String specialInstructions, String messageId) {

        LOGGER.info("Start Extract COFID {}", messageId);
        long cofId = 0L;
        String regex = "(?:COFID[:;\\|])\\s{0,1}(?<COFID>[0-9]*)";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        Matcher regexMatcher = pattern.matcher(specialInstructions);
        if (regexMatcher.find()) {

            cofId = Long.parseLong(regexMatcher.group("COFID").length() > 0 ?
                    regexMatcher.group("COFID") : "0");
        }
        LOGGER.info("End extracting COFID {}", messageId);
        return cofId;
    }

    public boolean insertFloristOrdersFallback(ErosOrderRequest erosOrder) {

        LOGGER.error("Insert Florist Orders {}, was not successfull. Retruning a fallback", erosOrder);

        return false;
    }


    public boolean updateFloristOrderStatusFallback(OrderStatusRequest orderStatusRequest) {

        LOGGER.error("update Florist Order status {}, was not successfull. Retruning a fallback", orderStatusRequest);

        return false;
    }

}