package com.ftd.erosingestionservice.service;

import com.ftd.erosingestionservice.api.entity.OrderEntity;
import com.ftd.erosingestionservice.api.repository.OrderRepository;
import com.ftd.erosingestionservice.api.request.Address;
import com.ftd.erosingestionservice.api.request.ErosOrderRequest;
import com.ftd.erosingestionservice.api.request.Florist;
import com.ftd.erosingestionservice.api.request.OrderStatusRequest;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;

import static org.mockito.Mockito.any;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class ErosOrderServiceTest {

    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private ErosOrderServiceImpl erosOrderService;

    private static final double ORDERPRICE = 23.33d;
    private static final long COFID = 100071105L;
    private Florist sender;
    private Florist receiver;
    private Address recipient;
    private ErosOrderRequest erosOrderRequest;
    private OrderStatusRequest orderStatusRequest;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        sender = new Florist();
        sender.setMemberCode("90-9086AA");
        sender.setMemberClassification("FLORIST");
        sender.setBusinessName("Test Sender Flowers");
        sender.setAddress("123 Test Rd");
        sender.setCity("Chicago");
        sender.setStateCode("IL");
        sender.setPostalCode("60616");
        sender.setPhone("1234567890");

        receiver = new Florist();
        receiver.setMemberCode("90-9086AB");
        receiver.setMemberClassification("FLORIST");
        receiver.setBusinessName("Test Receiver Flowers");
        receiver.setAddress("101 Test Rd");
        receiver.setCity("Downers Grove");
        receiver.setStateCode("IL");
        receiver.setPostalCode("60515");
        receiver.setPhone("9876543210");

        recipient = new Address();
        recipient.setName("Test Test");
        recipient.setAddress("3113 Woodcreek Dr");
        recipient.setCityStateZip("Downers Grove, IL 60515");
        recipient.setCity("Downers Grove");
        recipient.setStateCode("IL");
        recipient.setPostalCode("60515");
        recipient.setCountryCode("US");
        recipient.setPhone("1231234567");

        erosOrderRequest = new ErosOrderRequest();
        erosOrderRequest.setMainMemberCode("90-9086AA");
        erosOrderRequest.setMessageId("1116001");
        erosOrderRequest.setMessageReceivedDate("");
        erosOrderRequest.setMessageStatus("SENT");
        erosOrderRequest.setReferenceNumber("A1001T");
        erosOrderRequest.setOrderSequenceNumber("0001");
        erosOrderRequest.setDeliveryDate("2018-07-15");
        erosOrderRequest.setOccasionCode("BIRTHDAY");
        erosOrderRequest.setDeliveryDetails("AM Delivery");
        erosOrderRequest.setFirstChoiceProductCode("0001");
        erosOrderRequest.setFirstChoiceProductDescription("Test Flowers Description");
        erosOrderRequest.setSecondChoiceProductCode("");
        erosOrderRequest.setSecondChoiceProductDescription("");
        erosOrderRequest.setPrice(ORDERPRICE);
        erosOrderRequest.setCardMessage("Test Card Message");
        erosOrderRequest.setSpecialInstructions("Test special Instructions");
        erosOrderRequest.setOperator("Integration Test");
        erosOrderRequest.setSender(sender);
        erosOrderRequest.setReceiver(receiver);
        erosOrderRequest.setRecipient(recipient);
    }

    @Test
    public void testInsertFloristOrders() {

        //Arrange
        OrderEntity mockOrderResponse = new OrderEntity("90-9085AA",
                "SENT",
                false,
                DateTime.now(),
                true,
                COFID,
                DateTime.now(),
                DateTime.now(),
                erosOrderRequest);
        mockOrderResponse.setId("1bedc721ard1a46c8cad4200");

        when(orderRepository.save(any(OrderEntity.class))).thenReturn(mockOrderResponse);

        //Act
        boolean expected = erosOrderService.insertFloristOrders(erosOrderRequest);

        //Assert
        assertEquals(expected, true);
    }

    @Test
    public void whenFolOrderThenGetCofIdAndInsertOrder() {

        //Arrange
        OrderEntity mockResponse = new OrderEntity("90-9086AA",
                "SENT",
                false,
                DateTime.now(),
                true,
                COFID,
                DateTime.now(),
                DateTime.now(),
                erosOrderRequest);
        mockResponse.setId("1bedc721ard1a46c8cad4201");

        sender.setMemberClassification("FOL");
        erosOrderRequest.setSpecialInstructions("TIME REC: SUNDAY / 12:17PM CST\\r\\n" +
                "ORDER NUMBER INFO 123456789 / ABC9876543 1 of 1 ITEMS\\r\\nPROMOCODE: \\r\\n" +
                "RECIPE: Asiatic Lilies, roses, gilly flower, carnations, daisies, button poms, and greens.\\r\\n" +
                "WEB GIFT: N\\r\\nWEB GIFT ITEM: N\\r\\nIN STORE: Y\\r\\nDELIVERY INSTRUCTIONS: (Item 1 of 1)\\r\\n" +
                "RECIPIENT PHONE# : 630-111-2222\\r\\nSEND-TO COUNTRY : USA\\r\\nCREDIT CARD EXP : 12/2050\\r\\n" +
                "OCCASION: Birthday\\r\\nCUSTOMER NAME: John Doe\\r\\nAD: 123 MAIN STREET\\r\\n" +
                "AD: Downers Grove, IL 60515\\r\\nDAYTIME TEL. #: 6302223333\\r\\n" +
                "NIGHT TEL. #: 6303334444\\r\\nE-MAIL ADDRESS: hello_world_2018@domain.com\\r\\n" +
                "CREDIT CARD #: ************1111 TYPE: VI EX: 12.50\\r\\nITEM PRICE: 54.99\\r\\n" +
                "TIERED PRICING: Good Price\\r\\nDELIVERY: 0.00 SERVICE: 9.99 TAX: 5.36\\r\\nDISCOUNT: 0\\r\\n" +
                "ITEM TOTAL: 70.34 GRAND TOTAL: 70.34\\r\\n" +
                "HOW DID YOU HEAR ABOUT US? Other - See Comments -- ONLINE\\r\\nSITE ID: \\r\\n" +
                "CC MESSAGE: CARD NOT PROCESSED. Credit Card must be processed in store.\\r\\nAVS RESPONSE: \\r\\n" +
                "TRANSACTION ID: \\r\\nRESPONSE CODE: \\r\\nAPPROVAL CODE: \\r\\nCOFID: 100071105\\r\\nCOFSTATUS: 2");

        when(orderRepository.save(any(OrderEntity.class))).thenReturn(mockResponse);

        //Act
        boolean expected = erosOrderService.insertFloristOrders(erosOrderRequest);

        //Assert
        assertEquals(expected, true);
    }

    @Test
    public void testUpdateFloristOrder() {

        ArrayList<OrderEntity> mockOrder = new ArrayList<>(1);
        mockOrder.add(new OrderEntity("90-9086AA",
                "SENT",
                false,
                DateTime.now(),
                true,
                COFID,
                DateTime.now(),
                DateTime.now(),
                erosOrderRequest
        ));
        mockOrder.get(0).setId("1234");
        OrderEntity mockDbResponse = new OrderEntity("90-9086AA",
                "SENT",
                false,
                DateTime.now(),
                true,
                COFID,
                DateTime.now(),
                DateTime.now(),
                erosOrderRequest
        );
        mockDbResponse.setId("123334455");

        orderStatusRequest = new OrderStatusRequest();
        orderStatusRequest.setMainMemberCode("90-9086AA");
        orderStatusRequest.setMessageId("12345");
        orderStatusRequest.setMessageStatus("REJECTED");

        Sort sort = new Sort(Sort.Direction.DESC, "order.transmissionId");

        when(orderRepository.findOrderEntitiesByMainMemberCodeAndMessageId(
                "90-9086AA", "12345", sort)).thenReturn(mockOrder);

        when(orderRepository.save(any(OrderEntity.class))).thenReturn(mockDbResponse);

        boolean expected = erosOrderService.updateFloristOrderStatus(orderStatusRequest);

        assertEquals(expected, true);
    }

    @Test
    public void whenNoRecordInDatabaseThenReturnFalse() {

        orderStatusRequest = new OrderStatusRequest();
        orderStatusRequest.setMainMemberCode("90-9086AA");
        orderStatusRequest.setMessageId("12345");
        orderStatusRequest.setMessageStatus("REJECTED");

        Sort sort = new Sort(Sort.Direction.DESC, "order.transmissionId");

        //OrderEntity mockResult = mock(OrderEntity.class);
        ArrayList<OrderEntity> mockResult = new ArrayList<>();

        when(orderRepository.findOrderEntitiesByMainMemberCodeAndMessageId(
                "90-9086AA", "12345", sort)).thenReturn(mockResult);

        boolean expected = erosOrderService.updateFloristOrderStatus(orderStatusRequest);

        assertEquals(expected, false);
    }
}
