package com.ftd.erosingestionservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ftd.erosingestionservice.api.request.ErosOrderRequest;
import com.ftd.erosingestionservice.api.request.OrderStatusRequest;
import com.ftd.erosingestionservice.service.ErosOrderServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = ErosIngestionController.class, secure = false)
public class ErosIngestionControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ErosOrderServiceImpl erosOrderService;

    private JacksonTester<ErosOrderRequest> jsonErosOrderRequest;
    private JacksonTester<OrderStatusRequest> jsonOrderStatusRequest;

    private static final double ORDERPRICE = 23.33d;

    private ErosOrderRequest erosOrderRequest;
    private OrderStatusRequest orderStatusRequest;

    @Before
    public void setUp() {
        JacksonTester.initFields(this, objectMapper);
    }

    @Test
    public void postOrdersTest() throws Exception {

        erosOrderRequest = new ErosOrderRequest();
        erosOrderRequest.setMainMemberCode("90-9085AA");
        erosOrderRequest.setMessageId("12223");
        erosOrderRequest.setTransmissionId("1355346");

        final String orderRequestJson = jsonErosOrderRequest.write(erosOrderRequest).getJson();

        Mockito.when(erosOrderService.insertFloristOrders(any(ErosOrderRequest.class))).thenReturn(true);

        mockMvc.perform(
                post("/api/floristOrder/postOrders").content(orderRequestJson)
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
    }

    @Test
    public void whenMainMemberCodeIsNullThenThrowBadRequest() throws Exception {

        erosOrderRequest = new ErosOrderRequest();
        final String orderRequestJson = jsonErosOrderRequest.write(erosOrderRequest).getJson();

        mockMvc.perform(
                post("/api/floristOrder/postOrders").content(orderRequestJson)
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenExceptionThenThrowServerError() throws Exception {
        erosOrderRequest = new ErosOrderRequest();
        erosOrderRequest.setMainMemberCode("90-9085AA");
        erosOrderRequest.setMessageId("12333");
        erosOrderRequest.setTransmissionId("32343");
        final String orderRequestJson = jsonErosOrderRequest.write(erosOrderRequest).getJson();

        Mockito.when(erosOrderService.insertFloristOrders(any(ErosOrderRequest.class)))
                .thenThrow(new RuntimeException());

        mockMvc.perform(
                post("/api/floristOrder/postOrders").content(orderRequestJson)
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isInternalServerError());
    }

    @Test
    public void whenResponseIsFalseThenReturnBadRequest() throws Exception {

        erosOrderRequest = new ErosOrderRequest();
        erosOrderRequest.setMainMemberCode("90-9085AB");

        final String orderRequestJson = jsonErosOrderRequest.write(erosOrderRequest).getJson();

        Mockito.when(erosOrderService.insertFloristOrders(any(ErosOrderRequest.class))).thenReturn(false);

        mockMvc.perform(
                post("/api/floristOrder/postOrders").content(orderRequestJson)
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void postOrderStatusTest() throws Exception {

        orderStatusRequest = new OrderStatusRequest();
        orderStatusRequest.setMainMemberCode("90-9085AA");
        orderStatusRequest.setMessageId("123456789");
        orderStatusRequest.setMessageStatus("REJECTED");

        final String requestJson = jsonOrderStatusRequest.write(orderStatusRequest).getJson();

        Mockito.when(erosOrderService.updateFloristOrderStatus(any(OrderStatusRequest.class))).thenReturn(true);

        mockMvc.perform(
                post("/api/floristOrder/postOrderStatus").content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
    }

    @Test
    public void whenExceptionOccursInPostOrderStatusThenReturnServerError() throws Exception {

        orderStatusRequest = new OrderStatusRequest();
        orderStatusRequest.setMainMemberCode("10-1093AA");
        orderStatusRequest.setMessageId("21334");
        orderStatusRequest.setMessageStatus("REJECTED");

        final String requestJson = jsonOrderStatusRequest.write(orderStatusRequest).getJson();

        Mockito.when(erosOrderService.updateFloristOrderStatus(any(OrderStatusRequest.class)))
                .thenThrow(RuntimeException.class);

        mockMvc.perform(
                post("/api/floristOrder/postOrderStatus").content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isInternalServerError());
    }

    @Test
    public void whenUpdateOrderStatusResponseIsFalseReturnBadRequest() throws Exception {

        orderStatusRequest = new OrderStatusRequest();
        orderStatusRequest.setMainMemberCode("10-1098AA");
        orderStatusRequest.setMessageId("1233");
        orderStatusRequest.setMessageStatus("REJECTED");

        final String requestJson = jsonOrderStatusRequest.write(orderStatusRequest).getJson();

        Mockito.when(erosOrderService.updateFloristOrderStatus(any(OrderStatusRequest.class)))
                .thenReturn(false);

        mockMvc.perform(
                post("/api/floristOrder/postOrderStatus").content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }
}
